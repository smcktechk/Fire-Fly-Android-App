#include <Wire.h>

#define    MPU9250_ADDRESS            0x68

// Gyro scales
#define    GYRO_FULL_SCALE_1000_DPS   0x10

// Accel Scales
#define    ACC_FULL_SCALE_4_G        0x08


long t_previous; // time last sample was taken

// original is 50
long T = 50;     // sampling period

bool inPeak = fal se;

int GreenLed = 13;


// Declare Filter Variables
float aXx0, aXx1, aXx2, aXx3, aXx4, aXy0, aXy1, aXy2, aXy3, aXy4 = 0; // Accel X
float aYx0, aYx1, aYx2, aYx3, aYx4, aYy0, aYy1, aYy2, aYy3, aYy4 = 0; // Accel Y
float aZx0, aZx1, aZx2, aZx3, aZx4, aZy0, aZy1, aZy2, aZy3, aZy4 = 0; // Accel Z
float gXx0, gXx1, gXx2, gXx3, gXx4, gXy0, gXy1, gXy2, gXy3, gXy4 = 0; // Gyro X
float gYx0, gYx1, gYx2, gYx3, gYx4, gYy0, gYy1, gYy2, gYy3, gYy4 = 0; // Gyro Y
float gZx0, gZx1, gZx2, gZx3, gZx4, gZy0, gZy1, gZy2, gZy3, gZy4 = 0; // Gyro Z


// This function read Nbytes bytes from I2C device at address Address.
// Put read bytes starting at register Register in the Data array.
void I2Cread(uint8_t Address, uint8_t Register, uint8_t Nbytes, uint8_t* Data)
{
  // Set register address
  Wire.beginTransmission(Address);
  Wire.write(Register);
  Wire.endTransmission();

  // Read Nbytes
  Wire.requestFrom(Address, Nbytes);
  uint8_t index = 0;
  while (Wire.available())
    Data[index++] = Wire.read();
}

// Write a byte (Data) in device (Address) at register (Register)
void I2CwriteByte(uint8_t Address, uint8_t Register, uint8_t Data)
{
  // Set register address
  Wire.beginTransmission(Address);
  Wire.write(Register);
  Wire.write(Data);
  Wire.endTransmission();
}


// Initial time
long int ti;
volatile bool intFlag = false;

// Initializations
void setup()
{
  pinMode(GreenLed, OUTPUT);
  // Arduino initializations
  Wire.begin();
  Serial.begin(115200);

  Serial.print("Executing Code Now");

  // Set accelerometers low pass filter at 10Hz
  I2CwriteByte(MPU9250_ADDRESS, 25, 0x06);
  // Set gyroscope low pass filter at 5Hz
  I2CwriteByte(MPU9250_ADDRESS, 26, 0x06);


  // Configure gyroscope range
  I2CwriteByte(MPU9250_ADDRESS, 27, GYRO_FULL_SCALE_1000_DPS);
  // Configure accelerometers range
  I2CwriteByte(MPU9250_ADDRESS, 28, ACC_FULL_SCALE_4_G);

  // Store initial time
  ti = millis();
}


int GyPeakCount = 0;
int GzPeakCount = 0;


// Main loop, read and display data
#define V 20// Sample of max peaks
double AxMax = 0;
double InterimAxMax = -1000000;
int v = 0;


void loop()
{


  int count = 0;

  if (millis() - t_previous >= T)
  {
    // update time of last sample to current time
    t_previous = t_previous + T;






    // Display time
    //Serial.print (millis() - ti, DEC);
    //Serial.print ("\t");

    // ____________________________________
    // :::  accelerometer and gyroscope :::

    // Read accelerometer and gyroscope
    uint8_t Buf[14];
    I2Cread(MPU9250_ADDRESS, 0x3B, 14, Buf);

    // Create 16 bits values from 8 bits data

    // Accelerometer
    int16_t ax = -(Buf[0] << 8 | Buf[1]);
    int16_t ay = -(Buf[2] << 8 | Buf[3]);
    int16_t az = Buf[4] << 8 | Buf[5];

    // Gyroscope
    int16_t gx = -(Buf[8] << 8 | Buf[9]);
    int16_t gy = -(Buf[10] << 8 | Buf[11]);
    int16_t gz = Buf[12] << 8 | Buf[13];


    // Scale values
    float Ax = (ax / 8192.0);
    float Ay = (ay / 8192.0);
    float Az = (az / 8192.0);
    float Gx = (gx / 32.8);
    float Gy = (gy / 32.8);
    float Gz = (gz / 32.8);

    // Clibration
    //float Ax = sqrt(sq(Ax) + sq(Az));

    // Filter
    float AxF = AxStp(Ax);
    float AyF = AyStp(Ay);
    float AzF = AzStp(Az);
    float GxF = GxStp(Gx);
    float GyF = GyStp(Gy);
    float GzF = GzStp(Gz);

    // Create Gz Buffer
    float GzBuff[] = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};


    int n = 0;
    int N = 20;
    float Upper = 60.0;
    float Lower = -50.0;

    if    (n < 20 ) {

      GzBuff[n] = GyF;
    }
    if  (n = N)
    {

      //Count peaks of Gzm
      if (GzF > Upper) {
        if (!inPeak)
        {
          GzPeakCount ++;
          digitalWrite(GreenLed, HIGH);
          inPeak = true;
          /*Serial.print(Gy);
            Serial.print("/t");
            Serial.print(GzPeakCount); */
        }

      }
      else
      {
        digitalWrite(LED_BUILTIN, LOW);
        inPeak = false;
      }
      Serial.print (millis() - ti, DEC);
      Serial.print ("\t");
      Serial.print (Ax*100, 4);
      Serial.print ("\t");
      Serial.print (AxF*100, 4);
      Serial.print ("\t");
      /*Serial.print (Ay);
      Serial.print ("\t");
      Serial.print (AyF);
      Serial.print ("\t");
      Serial.print (Az, 4);
      Serial.print ("\t");
      Serial.print (AzF, 4);
      Serial.print ("\t");
      Serial.print (Gx, 4);
      Serial.print ("\t");
      Serial.print (GxF, 4);
      Serial.print ("\t");
      Serial.print (Gy, 4);
      Serial.print ("\t");
      Serial.print (GyF, 4);
      Serial.print ("\t");*/
      Serial.print (Gz);
      Serial.print ("\t");
      Serial.print (GzF);
      Serial.print ("\t");
      Serial.print (GzPeakCount);
      Serial.print ("\t");
      Serial.print (InterimAxMax, 4);
      n = 0;
      v++;
      if (v == V)
      {
        AxMax = InterimAxMax;
        InterimAxMax = -1000000;
        v = 0;
        //Serial.print(AxMax);
        //Serial.print("\t");
      }
      if (InterimAxMax < AxF)
      {
        InterimAxMax = AxF;
      }
    }
    else {
      Serial.print("Error!!!");
    }
    // End of line
    Serial.println("");
  }
}
double Max(double *ary, int len)
{
  double CurrentMax = -1000000; // Large Negative Number
  int Index;
  for (Index = 0; Index < len; Index ++)
  {
    //Serial.print(ary[Index]);
    //Serial.print(" ");
    if (ary[Index] > CurrentMax)
      CurrentMax = ary[Index];
  }
  return CurrentMax;
}

double Min(double *ary, int len)
{
  double CurrentMin = +1000000; // Large Number
  int Index;
  for (Index = 0; Index < len; Index ++)
  {
    //Serial.print(ary[Index]);
    //Serial.print(" ");
    if (ary[Index] < CurrentMin)
      CurrentMin = ary[Index];
  }
  return CurrentMin;
}

float AxStp(float p) //Filter Function
{

  //double x[5], y[5];
  aXx4 = aXx3;
  aXx3 = aXx2;
  aXx2 = aXx1;
  aXx1 = aXx0;
  aXx0 =  p;

  aXy4 = aXy3;
  aXy3 = aXy2;
  aXy2 = aXy1;
  aXy1 = aXy0;
  aXy0 =    (0.0196  * aXx0)
            + (0.0324  * aXx1)
            + (0.0439  * aXx2)
            + (0.0324  * aXx3)
            + (0.0196  * aXx4)
            + (2.0583  * aXy1)
            - (1.8639  * aXy2)
            + (0.7916  * aXy3)
            - (0.1340  * aXy4);

  /*
     b = 0.0196    0.0324    0.0439    0.0324    0.0196
     a = 1.0000   -2.0583    1.8639   -0.7916    0.1340
  */

  return

    aXy0;

}

float AyStp(float q) //Filter Function
{

  //double x[5], y[5];
  aYx4 = aYx3;
  aYx3 = aYx2;
  aYx2 = aYx1;
  aYx1 = aYx0;
  aYx0 =  q;

  aYy4 = aYy3;
  aYy3 = aYy2;
  aYy2 = aYy1;
  aYy1 = aYy0;
  aYy0 =    (0.0196  * aYx0)
            + (0.0324  * aYx1)
            + (0.0439  * aYx2)
            + (0.0324  * aYx3)
            + (0.0196  * aYx4)
            + (2.0583  * aYy1)
            - (1.8639  * aYy2)
            + (0.7916  * aYy3)
            - (0.1340  * aYy4);

  /*
     b = 0.0196    0.0324    0.0439    0.0324    0.0196
     a = 1.0000   -2.0583    1.8639   -0.7916    0.1340
  */

  return

    aYy0;

}

float AzStp(float u) //Filter Function
{

  //double x[5], y[5];
  aZx4 = aZx3;
  aZx3 = aZx2;
  aZx2 = aZx1;
  aZx1 = aZx0;
  aZx0 =  u;

  aZy4 = aZy3;
  aZy3 = aZy2;
  aZy2 = aZy1;
  aZy1 = aZy0;
  aZy0 =      (0.0196  * aZx0)
              + (0.0324  * aZx1)
              + (0.0439  * aZx2)
              + (0.0324  * aZx3)
              + (0.0196  * aZx4)
              + (2.0583  * aZy1)
              - (1.8639  * aZy2)
              + (0.7916  * aZy3)
              - (0.1340  * aZy4);

  /*
     b = 0.0196    0.0324    0.0439    0.0324    0.0196
     a = 1.0000   -2.0583    1.8639   -0.7916    0.1340
  */

  return

    aZy0;

}

float GxStp(float h) //Filter Function
{

  //double x[5], y[5];
  gXx4 = gXx3;
  gXx3 = gXx2;
  gXx2 = gXx1;
  gXx1 = gXx0;
  gXx0 =  h;

  gXy4 = gXy3;
  gXy3 = gXy2;
  gXy2 = gXy1;
  gXy1 = gXy0;
  gXy0 =    (0.0196  * gXx0)
            + (0.0324  * gXx1)
            + (0.0439  * gXx2)
            + (0.0324  * gXx3)
            + (0.0196  * gXx4)
            + (2.0583  * gXy1)
            - (1.8639  * gXy2)
            + (0.7916  * gXy3)
            - (0.1340  * gXy4);

  /*
     b = 0.0196    0.0324    0.0439    0.0324    0.0196
     a = 1.0000   -2.0583    1.8639   -0.7916    0.1340
  */

  return

    gXy0;

}

float GyStp(float j) //Filter Function
{

  //double x[5], y[5];
  gYx4 = gYx3;
  gYx3 = gYx2;
  gYx2 = gYx1;
  gYx1 = gYx0;
  gYx0 =  j;

  gYy4 = gYy3;
  gYy3 = gYy2;
  gYy2 = gYy1;
  gYy1 = gYy0;
  gYy0 =    (0.0196  * gYx0)
            + (0.0324  * gYx1)
            + (0.0439  * gYx2)
            + (0.0324  * gYx3)
            + (0.0196  * gYx4)
            + (2.0583  * gYy1)
            - (1.8639  * gYy2)
            + (0.7916  * gYy3)
            - (0.1340  * gYy4);

  /*
     b = 0.0196    0.0324    0.0439    0.0324    0.0196
     a = 1.0000   -2.0583    1.8639   -0.7916    0.1340
  */

  return

    gYy0;

}

float GzStp(float k) //Filter Function
{

  //double x[5], y[5];
  gZx4 = gZx3;
  gZx3 = gZx2;
  gZx2 = gZx1;
  gZx1 = gZx0;
  gZx0 =  k;

  gZy4 = gZy3;
  gZy3 = gZy2;
  gZy2 = gZy1;
  gZy1 = gZy0;
  gZy0 =    (0.0196    * gZx0)
            + (0.0324  * gZx1)
            + (0.0439  * gZx2)
            + (0.0324  * gZx3)
            + (0.0196  * gZx4)
            + (2.0583  * gZy1)
            - (1.8639  * gZy2)
            + (0.7916  * gZy3)
            - (0.1340  * gZy4);

  /*
     b = 0.0196    0.0324    0.0439    0.0324    0.0196
     a = 1.0000   -2.0583    1.8639   -0.7916    0.1340
  */

  return
    gZy0;
}
